# -*- coding: utf-8 -*-

import logging
import requests
import configparser
import os.path
from connectors import IdProvider

class VisID(IdProvider):
    orgname = "vis"

    def __init__(self):
        """ Prepares the connector to the VIS REST Interface."""
        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.loglevel)

        # Read in VIS configuration
        config = self.get_config('vis')
        self.status_url = config.get("visstatus", "status_url")
        self.dispense_url = config.get("visstatus", "dispense_url")
        self.api_key = config.get("visstatus", "api_key")


    def auth(self, rfidnr: int) -> int:
        self.logger.debug("Trying to authenticate RFID %s...", str(rfidnr))

        rfid = "%(rfidnr)s@rfid.ethz.ch" % dict (rfidnr=str(rfidnr))
        payload = {'key': self.api_key, 'rfid': rfid}

        r = requests.post(self.status_url, data=payload, timeout=5)

        if r.status_code is not 200:
            self.logger.debug('VIS API returned with {} (1)'.format(r.status_code))
            return None

        return int(r.text)


    def report(self, rfidnr: int) -> bool:
        rfid = "%(rfidnr)s@rfid.ethz.ch" % dict (rfidnr=str(rfidnr))
        payload = {'key': self.api_key, 'rfid': rfid}

        r = requests.post(self.dispense_url, data=payload, timeout=5)

        if r.status_code is not 200:
            self.logger.info('VIS API returned with {} (2)'.format(r.status_code))

        return r.status_code == 200
