import os
import logging
from configparser import ConfigParser

class IdProvider(object):
    """ Base interface for identity providers.
    
    To be used by the BeerController, any connector to an identity provider has
    to implement this interface
    Every identity provider inheriting from this interface
    """
    def __init__(self):
        """ Initialization of the connector.
        """
        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.loglevel)


    def __repr__(self):
        return '<IdentityProvider %s>' % (self.__class__.__name__)


    def __str__(self):
        return self.orgname


    def __unicode__(self):
        return str(self.orgname)


    @property
    def orgname(self):
        raise NotImplementedError(
            "Attribute 'orgname' must be set by class '%s'" % (
                self.__class__.__name__
            )
        )


    def get_config(self, config_name: str) -> ConfigParser:
        """Load configuration values from a file.
        
        Args:
            config_name: Name of the configuration file without file ending!
                        (e.g. «amiv» for the file amiv.conf)
        """
        here = os.path.abspath(os.path.dirname(__file__))
        config_path = os.path.join(here, '../config/', config_name + '.conf')

        config = ConfigParser()
        if not config.read(config_path):
            logging.error('Config: could not read config file "' + config_name + '"')
        else:
            return config


    def auth(self, rfid):
        """ Authenticates the given legi number if possible.

        Args:
            rfid: The six digit RFID number (int as str).

        Returns:
            True if authentication was successful, False otherwise.
        """
        raise NotImplementedError(
            "Method 'auth' must be implemented by class '%s'" % (
                self.__class__.__name__
            )
        )

    def report(self, rfid) -> bool:
        """ Reports a vending from the given user.
        
        Args:
            rfid: rfid of the user
        
        Returns:
            True if reporting was successful, False otherwise.
        """
        raise NotImplementedError(
            "Method 'report' must be implemented by class '%s'" % (
                self.__class__.__name__
            )
        )


from .amiv import AmivID
from .vis import VisID
from .vmp import VmpID

IdProviders = (VmpID, VisID, AmivID)
