# -*- coding: utf-8 -*-

import json
import requests

import configparser
import datetime
import logging
import os.path

from connectors import IdProvider
from utils import get_config


class AmivID(IdProvider):
    """ Connector to the AMIV Beerlog Rest Interface.

    Attributes:
        orgname: "amiv"
    """
    orgname = "amiv"

    def __init__(self):
        """ Prepares the connector to the AMIV Beerlog REST Interface."""
        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.loglevel)

        # Read in AMIV Beerlog configuration
        config = self.get_config('amiv')

        self.__host = config.get('beerlog', 'host')
        self.__apikey = config.get('beerlog', 'apikey')
        self.__product = config.get('beerlog', 'product')


    def auth(self, rfid: int) -> int:
        """ Authenticates the given rfid number with the AMIV Beerlog API

        Args:
            rfid: The six digit RFID number.
        """
        self.logger.debug('Trying to authenticate RFID %s...', rfid)

        try:
            response = requests.get(
                '{}/api/check/{}'.format(self.__host, rfid),
                headers={'Authorization': self.__apikey}, timeout=3)

            if response.status_code == 404:
                self.logger.debug('AMIV Beerlog does not know RFID %i.', rfid, exc_info=True)
            elif response.status_code != 200:
                self.logger.info(
                    'AMIV Beerlog returned status code "%i" for RFID %s',
                    response.status_code, rfid, exc_info=True)
            else:
                data = response.json()
                return data.get('beer', 0)
        except Exception as e:
            self.logger.error(e)
        return None

    def report(self, rfid: int, organisation: str = 'amiv') -> bool:
        data = {'rfid': str(rfid), 'product': 'beer', 'organisation': organisation}

        try:
            response = requests.post(
                '{}/api/report'.format(self.__host),
                json=data, headers={'Authorization': self.__apikey, 'Content-Type': 'application/json'}, timeout=10)

            if response.status_code == 404:
                self.logger.debug('AMIV Beerlog does not know RFID %s.', rfid, exc_info=True)
            elif response.status_code != 201:
                self.logger.error(
                    'AMIV Beerlog returned status code "%i" for RFID %s',
                    response.status_code, rfid, exc_info=True)
            else:
                return True
        except Exception as e:
            self.logger.error(e)
        return False
