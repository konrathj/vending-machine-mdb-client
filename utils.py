import os
import logging
from configparser import ConfigParser


def get_config(config_name: str) -> ConfigParser:
    """Load configuration values from a file.

    Args:
        config_name: Name of the configuration file without file ending!
                    (e.g. «amiv» for the file amiv.conf)
    """
    here = os.path.abspath(os.path.dirname(__file__))
    config_path = os.path.join(here, 'config/', config_name + '.conf')

    config = ConfigParser()
    if not config.read(config_path):
        logging.error('Config: could not read config file "' + config_name + '"')
    else:
        return config
