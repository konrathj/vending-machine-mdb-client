# import os
from threading import Thread, Lock
from typing import Union, Tuple, Callable
from configparser import ConfigParser
import logging
import time
import evdev
from utils import get_config


class Legireader(Thread):
    """Thread class to handle inputs from LEGIC NFC reader."""


    def __init__(self,
            config: ConfigParser,
            handle_start_reading: Callable[[], None],
            handle_read_success: Callable[[str, int], None],
            handle_read_failure: Callable[[], None]):
        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.loglevel)

        self.reader_lock = Lock()
        self.is_running = False
        self.reading = False
        self.stamp = config.get('legireader', 'stamp')
        self.stamp_index = int(config.get('legireader', 'stampIndex'))
        self.uid_index = int(config.get('legireader', 'uidIndex'))
        self.uid_length = int(config.get('legireader', 'uidLength', fallback='8'))
        self.deviceName = config.get('legireader', 'deviceName', fallback='OEM RFID Device (Keyboard)')
        self.handle_start_reading = handle_start_reading
        self.handle_read_success = handle_read_success
        self.handle_read_failure = handle_read_failure

        Thread.__init__(self, daemon=True)

        self.setupRFIDReader()


    def setupRFIDReader(self) -> None:
        """ Setup of RFID Reader (Keyboard device)."""
        # set up RFID device by identification of its name
        devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
        if self.deviceName not in [device.name for device in devices]:
            self.logger.error('RFID reader not found in devices. Try finding it with sudo and set up its permissions via udev rule')
            self.reader = None
            return

        path = devices[[device.name for device in devices].index(self.deviceName)].path
        self.logger.info('Found RFID reader at '+path)
        try:
            self.reader = evdev.InputDevice(path)
        except Exception:
            self.logger.error('Could not open RFID reader.')
            self.reader = None
            return

        # get exclusive read on device via EVIOCGRAB
        self.reader.grab()
        self.flush()
        self.logger.info('Successfully connected, grabbed and flushed the RFID reader. Listening.')


    def reset(self) -> None:
        """ Reset thread to its initial state."""
        with self.reader_lock:
            if self.reader is not None:
                self.reader.ungrab()
                self.reader.close()
            self.setupRFIDReader()


    def run(self) -> None:
        """ Main loop of the thread handling reader input."""

        if self.reader is None:
            self.logger.warning('RFID Reader is None!')
            return

        self.is_running = True
        reading_active = False

        while self.is_running:
            try:
                # read the rfid reader and validate the raw data
                reading_active = True
                raw_data = self.poll()
                if raw_data is not None:
                    self.logger.debug('Processing raw data from rfid reader.')
                    (uid, rfid) = self.extract(raw_data)
                    if rfid is not None:
                        # if a valid rfid was found, store it as latest read
                        self.logger.debug('detected rfid: ' + str(rfid))
                        self.logger.debug('detected uid: ' + str(uid))
                        reading_active = False
                        if self.handle_read_success:
                            self.handle_read_success(uid, rfid)
                    else:
                        reading_active = False
                        if self.handle_read_failure:
                            self.handle_read_failure()
            except Exception as e:
                self.logger.exception('exception: {}'.format(e))
            if reading_active and self.handle_read_failure:
                self.handle_read_failure()

            # sleep to prevent hogging resources
            time.sleep(1)
            self.flush()


    def poll(self) -> Union[str, None]:
        """Read RFID reader input and return the preprocessed string"""

        data = ''

        self.logger.debug('Started polling ...')

        with self.reader_lock:
            for event in self.reader.read_loop():
                # only register keystrokes, and filter out Shift, append to data string
                if event.type is evdev.ecodes.EV_KEY and event.value is 1 and event.code is not evdev.ecodes.ecodes['KEY_LEFTSHIFT']:
                    value = evdev.ecodes.keys[event.code]
                    # Just start recording when receiving the letter «L»,
                    # which is the first letter of an expected message.
                    if len(data) == 0 and value != 'KEY_L':
                        pass
                    else:
                        if not self.reading:
                            self.reading = True
                            self.logger.debug('Start reading...')
                            if self.handle_start_reading:
                                self.handle_start_reading()

                        if value == 'KEY_ENTER':
                            data += '\n'
                        elif value == 'KEY_SPACE':
                            data += ' '
                        else:
                            data += value.replace('KEY_', '')
                # Stop recording if reaching the end of the message or if the length is enough,
                # so that all required data should be recorded. This way we can minimize the latency.
                if data.endswith('\nEND\n') or len(data) > 44:
                    self.logger.debug('Found end of reader output or read enough data.')
                    self.reading = False
                    return data

        if self.reading:
            self.reading = False
            return data
        return None


    def extract(self, raw_input: str) -> Tuple[str, int]:
        """Extracts the UID and RFID numbers
        
        Args:
            raw_input: preprocessed input string received from the RFID reader.
        """

        if raw_input is None:
            self.logger.debug('Input was None.')
            return (None, None)
        if len(raw_input) < 45:
            self.logger.debug('Input was too short.')
            return (None, None)
        if raw_input[0:5] != 'LEGIC':
            self.logger.info('RFID is not of type LEGIC, got '+str(raw_input[0:5]))
            return (None, None)
        if raw_input[self.stamp_index:self.stamp_index+len(self.stamp)] != self.stamp:
            self.logger.info('Found an unknown RFID stamp, got '+str(raw_input[self.stamp_index:self.stamp_index+len(self.stamp)]))

        self.logger.debug('RFID was valid.')
        rfid = int(raw_input[self.stamp_index+len(self.stamp):self.stamp_index+len(self.stamp)+6])
        uid = raw_input[self.uid_index:self.uid_index+self.uid_length]
        return (uid, rfid)


    def flush(self):
        """Consume all pending data in the queue from the reader input and discard it."""
        while self.reader.read_one() != None:
            pass
        self.logger.debug('Flushed.')


    def stop(self):
        """Terminate the thread."""
        self.logger.info("Stopping Legireader...")
        self.is_running = False
        if self.reader is not None:
            self.reader.ungrab()
            self.reader.close()


# ==========================================
# == Test Program for this specific class ==
# ==========================================

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s\t%(levelname)s\t[%(name)s: %(funcName)s]\t%(message)s',
                        datefmt='%Y-%m-%d %I:%M:%S')

    def start_reading():
        logging.info('RFID Card started reading!')
    
    def read_successful(uid: str, rfid: int):
        logging.info('Read Card with UID: ' + uid + ' and RFID: ' + str(rfid))

    def read_failure():
        logging.warning('Reading card failed!')

    config = get_config('base')
    rfid = Legireader(config, start_reading, read_successful, read_failure)
    rfid.start()

    try:  # try-block for KeyboardInterrupt
        while True:
            time.sleep(0.5)

    except KeyboardInterrupt:  # on CTRL-C, stop all threads and shut down
        rfid.stop()
        if rfid.isAlive():
            rfid.join(5.0)
