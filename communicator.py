import queue
import requests
from threading import Thread, Lock
from configparser import ConfigParser
from typing import Union, Tuple, Callable
import logging
import os
import time

from connectors import IdProviders


AMPEL_CHECK_INTERVAL = 60  # in seconds


class Communicator(Thread):

    UNKNOWN_CARD = 2
    AMPEL_RED    = 1
    RESPONSE_OK  = 0

    def __init__(self, config: ConfigParser):
        # Level options: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self.loglevel = logging.INFO
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(self.loglevel)

        self.ampel_url = config.get('default', 'ampelURL')
        self.dispensed_beer = queue.Queue()
        self.running = False
        self.is_ampel_red = False
        self.ampel_check_time = 0
        self.providers_lock = Lock()
        self.providers = {}
        for connector in IdProviders:
            self.providers[connector.orgname] = connector()
        Thread.__init__(self)


    def reset(self) -> None:
        with self.providers_lock:
            self.providers = {}
            for connector in IdProviders:
                self.providers[connector.orgname] = connector()


    def run(self):
        self.running = True

        while self.running or not self.dispensed_beer.empty():
            if not self.dispensed_beer.empty():
                try:
                    (rfid, org) = self.dispensed_beer.get()

                    with self.providers_lock:
                        if not self.run_task_and_retry(lambda: self.providers[org].report(rfid)):
                            self.logger.error('REPORT failed for org {}, rfid {}'.format(org, rfid))
                            continue
                        if not self.run_task_and_retry(lambda: self.log_vend(rfid, org)):
                            self.logger.error('LOG failed for org {}, rfid {}'.format(org, rfid))
                            continue
                        self.logger.info('Reported vend for {} from org {}'.format(rfid, org))
                except Exception as e:
                    self.logger.exception('COMM: {}'.format(e))

            if time.time() - self.ampel_check_time > AMPEL_CHECK_INTERVAL:
                self.ampel_check_time = time.time()
                self.check_ampel_state()

            time.sleep(0.2)
        return True

    def stop(self):
        self.running = False

    def get_available_beer_amount(self, rfid: int) -> Tuple[int, int, Union[str, None]]:
        org = None
        beer = 0
        best_result = (self.UNKNOWN_CARD, 0, None)

        if self.is_ampel_red:
            return (self.AMPEL_RED, 0, None)

        for id_provider in list(self.providers.values()):
            try:
                beer = id_provider.auth(rfid)
                if beer is not None:
                    org = id_provider.orgname
                    self.logger.debug('%s from %s has %d beer available',
                                rfid, org, beer)
                    if best_result[0] is self.UNKNOWN_CARD or (best_result[1] == 0 and beer > 0):
                        best_result = (self.RESPONSE_OK, beer, org)
            except Exception as e:
                self.logger.exception(e)
        self.logger.info('Chosen org {}'.format(best_result))
        return best_result

    def check_ampel_state(self) -> bool:
        """ Checks the state of the Ampel at VIS """
        self.logger.debug('Checking Ampel status...')

        try:
            response = requests.get(self.ampel_url, timeout=10)

            if response.status_code != 200:
                self.logger.error(
                    'Ampel status returned status code "%i"',
                    response.status_code, exc_info=True)
            elif response.text == 'red':
                    self.is_ampel_red = True
                    return
        except Exception as e:
            self.logger.exception(e)
        self.is_ampel_red = False

    def report_dispensed_beer(self, rfid: int, org: str) -> None:
        self.dispensed_beer.put((rfid, org))


    def log_vend(self, rfid: int, org: str) -> bool:
        # The current amivid provider uses the log as the report
        # So if the org is amiv, this vending event has already been processed
        if org.lower() != 'amiv':
            return self.providers['amiv'].report(rfid, org)
        return True


    def run_task_and_retry(self, task: Callable[[], bool], max_retries: int = 3) -> bool:
        error_counter = 0
        while error_counter < max_retries:
            try:
                if task():
                    return True
            except Exception as e:
                self.logger.exception(e)
            error_counter += 1
        return False
