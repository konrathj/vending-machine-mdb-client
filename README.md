# Vending Machine MDB Client

## Requirements

You need an installation of Raspbian on a Raspberry Pi with the package `pigpio` installed.

Do not forget to install the python dependencies with `pip3 install -r requirements.txt`.
You should set up a python virtual environment with `virtualenv` beforehand.

## Installation

Create a new Systemd Service using the file `beer.service` in the root of this repository.

## Usage

Start the application with `python3 main.py`.

If the application seems to hang somewhere, please try to send SIGHUP to the process first.
If the main thread is still responding, it will completely reset the application's state.
